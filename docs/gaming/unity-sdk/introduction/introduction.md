---
id: introduction
disable_pagination: true
title: Introduction
slug: /gaming/unity-sdk/
author: Mathias Hiron and Efe Kucuk
---

## Tezos SDK for Unity

### Connect your games to Tezos & unlock new features in minutes. 

<p align="center">
<p> <img src="https://unity.com/sites/default/files/2022-09/unity-verified-solution-small.png"  width="30%" height="30%" /></p>
<small className="figure">Unity Verified Solution</small>
</p>


1. **Connect to a wallet**: Generate and connect wallets with state-of-art UX
2. **Access blockchain data**: Retrieve data and interact with the blockchain
3. **Invoke smart contracts**: Generate and interact with smart contracts
4. **Create independent in-game assets**: Enable users to fully own and verify in-game assets

>Tezos SDK for Unity is open-source and free forever. You can view the source code and contribute to it on [GitHub.](https://github.com/trilitech/tezos-unity-sdk/)


## Getting Started

Visit our [Getting Started](https://opentezos.com/gaming/unity-sdk/getting-started/) page to learn how to install and utilize the SDK with examples and sample projects.

>Looking for the shortcut?
>You can download the SDK directly by visiting our [GitHub Releases Page.](https://github.com/trilitech/tezos-unity-sdk/releases
)



## Platforms & Support
#### Supported platforms
- iOS
- Android
- Windows
- MacOS
- Browser (WebGL)

#### Supported Unity Versions
- 2021.3
- 2022.3

## Financial Grants & Support
The Tezos Foundation offers grants and further support for top games utilizing the Tezos SDK for Unity. Take this opportunity to bring your game to life and receive the backing you need to succeed.

>You can apply for a grant [here](https://tezos.foundation/grants/).

## Tezos Community Discord
Looking to get further support and share your work? Join or [Discord](https://discord.com/invite/hsSEtAb9er/) today and reach us out!



