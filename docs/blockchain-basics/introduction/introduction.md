---
id: introduction
disable_pagination: true
title: Introduction
slug: /blockchain-basics
authors: Mathias Hiron and Lucas Felli
---

Before we start digging into Tezos itself, let's review the basics of blockchain and cryptocurrencies.

At its heart, a blockchain is a piece of technology that enables a group of users to collectively share data, and has the following properties:

- any user in the group has access to all of the data
- any user can make changes to the data, as long they follow a set of rules
- no small subset of users should be able to control the system

This set of properties, associated with the right set of rules, can be leveraged for many use cases, including the secure storing and trading of cryptocurrencies and other digital assets.

In the following pages, we present the basics of an architecture that enables these properties.

We start by presenting the data structure of the blockchain that allows [decentralized storage](/blockchain-basics/decentralized-storage/), then present how users can work together to share this data and reach agreements on what changes are applied.

We then present how this architecture can be leveraged to create a [decentralized cryptocurrencty](/blockchain-basics/decentralized-currency/) where users can freely trade funds.

We present how a [consensus mechanism](/blockchain-basics/consensus-mechanism/) can be used to make sure no single entity holds too much power.

Finally, we explain how this architecture can be used to support [smart contracts](/blockchain-basics/smart-contracts/).
