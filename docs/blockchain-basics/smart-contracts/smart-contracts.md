---
id: smart-contracts
title: Smart Contracts
authors: Mathias Hiron and Lucas Felli
---

## Smart Contracts

Once we have a system for a P2P network of nodes to agree on a sequence of transactions to execute in a specific order, we can apply this system to all kinds of transactions.

### Virtual machine

If we push this to the extreme, we could imagine expanding transactions to anything a computer could possibly do, in other words, any piece of code. This would require for nodes to agree on a format to represent code (some programming language), and on an API, a defined set of functions that code can use to interact with its environment.

Of course, the type of code that any user may send in a transaction to be executed on every node has to be limited.

Try to think of what limitations would be required, before you continue.

Here are the main limitations required for this to work:

- a given transaction needs to have the **exact same effect** on every node, as their state need to be identical after each block, no matter where or when the execution happens.
- the **execution time** of a transaction needs to be limited, so that the whole network doesn't become slow
- the effect of a transaction sent by a given user has to be **restricted to what other users agree** they are allowed to do

This means all the code that will be run needs to be executed in some type of sandboxed **virtual machine**: a well-defined, limited environment, with no access to the outside world. Indeed, the "outside world", whether it's some external service or content on the machine or online, may be different from node to node, so accessing it could produce different results depending on the node or on when the execution takes place.

What's left is code that interacts with the data stored in the node itself. This can include not only the balances of accounts, but other types of data such as information about NFTs and their owners, scores of games, references to pieces of art, etc.

### Smart contracts and calls

For users to agree on what can be done without limiting use cases, the execution of code is divided in two steps:

- Storing pieces of code that are made publicly available.
 These are what we call **Smart Contracts**. They store their own dedicated data that can only be modified by them. The code of a contract may only modify this data, or emit its own new transactions (transferring tez or calling other smart contracts).
- Calling these smart contracts with given parameters, wich executes the code.

These are two new types of transactions on Tezos: **contract originations** (we also talk about deployment), and **contract calls**.

With this approach, someone can create a set of rules in the form of a smart contract, as to how its data is modified depending on who calls them with what parameters, then potential users can check the code of the contract and make sure they agree with its behavior, before they call them.

A smart contract may have multiple entrypoints, like the functions or methods of the contract, each with their own parameters and code.

When we call an entrypoint, we can pass our own parameters. Other information, such as who is making the transaction, how many tez they sent to the contract along with the call, or what the current date is, can be accessed to the code, along with the contract's own storage.

### Smart contract example

Here is a very basic example of smart contract:

Storage:

- owner: an address (of the current owner)
- price: a number of tez

Entrypoints:

- purchase()
    - check that the amount sent by the caller equals the price
    - send this amount to the current owner
    - change the owner to the address of the caller
- setPrice(newPrice)
    - check that the caller is the owner
    - set the price to newPrice

This contract in itself is a digital property that people can purchase or sell. A contract like this could also be used to authenticate the ownership of an associated physical item.

Once deployed, this contract can never be changed. This means that when a user calls the purchase entrypoint and send money, they can be certain that if the execution takes place, they will become the new owner of the contract, and be able to then set the price at which they are willing to sell it later.

Only the owner can change the price, and no one can ever prevent them from selling the ownership to whoever wants to purchase it.

Smart contracts open blockchains to all kinds of use cases, decentralized finance, art, gaming, decentralized identity and more.

Check the [smart contract pages](/smart-contracts/simple-nft-contract-1/) to learn more on this topic.
