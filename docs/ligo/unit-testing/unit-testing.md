---
id: unit-testing
title: Unit Testing
authors: Maxime Sallerin and Benjamin Pilia
---

LIGO has its own Testing Framework. You will be able to use same ligo language to test your ligo source code.

> Note : It is even possible to write the test in onatoehr ligo flavour, for example test written in mligo but source code in jsligo. It is because ligo languages are compatible.

## Unit Testing

Just like any other programming language, writing LIGO code (to be compiled into Michelson) is not the only task of a developer;
writing the tests is as important as writing the code.

Writing a test may take extra time, but some great benefits usually reward this effort:

- a test helps to understand the code: a user or a new developer can easily understand the code behavior through testing. The test name should clearly describe what the code is checking, and the test itself shows how the code should be handled.
- checking the good behavior of the written code. It benefits both the developer and the user of a smart contract: both want to be sure the smart contract behaves as it should.
- It also makes the code robust for future modifications, i.e. code refactoring or new features. The tests make sure that the behavior has not changed, and if it did, it would clearly outline where.
- Tests can easily be automated: they can be included in a CI/CD, which will run them after any push.

> **CI/CD (Continuous Integration / Continuous Delivery)** is a method to frequently deliver apps to customers by introducing automation into the stages of app development. The main concepts attributed to CI/CD are continuous integration, continuous delivery, and continuous deployment.

There are different types of tests, as described below in the automated testing strategy.
<br/>

![From bottom to top: unit tests, intrgration tests, functional tests, HMI tests, Security and perf tests. Execution time and costs go up with the pyramide. Quantity of tests and developer investments go up by going down the pyramide.](pyramide_of_tests.svg)
<small className="figure">FIGURE 1: Pyramide of Tests</small>

Unit tests are the base of the pyramid and therefore the most important part.

**Unit testing** is performed at a very fine granularity by fully verifying the behavior of a portion of code or by partially isolating it from its dependencies. It is therefore simple to write and maintain them. In contrast, an integration test aims to verify that several components work well together.

To go further, take a look at [Test-Driven Development (TDD)](https://en.wikipedia.org/wiki/Test-driven_development), which is a development method emphasizing the writing of automated tests as a tool to guide the implementation of features.

## Three ways of testing in Ligo

The LIGO command-line interpreter provides commands to directly test your LIGO code. The three main commands we currently support are:

- ligo run test : run unit tests for ligo test files
- ligo run interpret : to execute code interpretation on the terminal
- ligo run dry-run : simulata execution of source code on a node

## [=> Go to Ligo documentation chapter for examples <=](https://ligolang.org/docs/advanced/testing?lang=jsligo)
