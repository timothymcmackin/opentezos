# OpenTezos

Welcome Tezos Developers! Explore the technical and economic concepts behind the Tezos Network, experiment with our tutorials, or start building your own Tezos Dapp. Each module will teach you a full concept of Tezos.

See an error somewhere? Fix it with a GitLab Merge Request. OpenTezos is a constantly evolving platform that welcomes all readers' inputs. You can even add a new chapter or module if you wish.

## Installation

```console
yarn install
```

## Local Development

```console
yarn start:dev
```

## Production

```console
yarn start
```

## How to use variables inside .md files?

- Define variables inside [variables.json](variables.json), only upper case letters and  `_` are authorized.
- Write `[:VARIABLE_NAME:]` in any .md file.
- Use `@` if you need to, for example with `"FOO": "bar"`:
  - `[:FOO@CAP:]` would result in `Bar`
  - `[:FOO@UPP:]` would result in `BAR`
  - `[:FOO@LOW:]` would result in `bar`
